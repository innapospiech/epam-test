package pagefactory.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CartTests extends BaseTest {

    private final String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART = "1";
    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_ADDITIONAL_ITEM = "2";
    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_REMOVING_ITEM = "";

    @Test(priority = 1)
    public void checkAddToCart() {
        getHomePage().clickOnWomanCatalogButton();
        getBasePage().waitForPageLoadComplete(180);
        getHomePage().clickOnGhdStylingButton();
        getBasePage().waitForPageLoadComplete(60);
        getGhdStylingPage().clickOnGhdHotBrushButton();
        getBasePage().waitForPageLoadComplete(120);
        getGhdHotBrushPage().clickOnAddToCartButton();
        getBasePage().implicitWait(180);
        Assert.assertEquals(getHomePage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
    }

    @Test(priority = 2)
    public void checkAddAdditionalItemToCart() {
        getHomePage().clickOnWomanCatalogButton();
        getBasePage().waitForPageLoadComplete(180);
        getHomePage().clickOnGhdStylingButton();
        getBasePage().waitForPageLoadComplete(60);
        getGhdStylingPage().clickOnGhdHotBrushButton();
        getBasePage().waitForPageLoadComplete(180);
        getGhdHotBrushPage().clickOnAddToCartButton();
        getBasePage().implicitWait(300);
        getGhdHotBrushPage().clickOnAddToCartButton();
        getBasePage().implicitWait(300);
        Assert.assertEquals(getHomePage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_ADDITIONAL_ITEM);
    }

    @Test(priority = 3)
    public void checkDecreaseAmountOfItemsToCar(){
        getHomePage().clickOnWomanCatalogButton();
        getBasePage().waitForPageLoadComplete(180);
        getHomePage().clickOnGhdStylingButton();
        getBasePage().waitForPageLoadComplete(60);
        getGhdStylingPage().clickOnGhdHotBrushButton();
        getBasePage().waitForPageLoadComplete(240);
        getGhdHotBrushPage().clickOnAddToCartButton();
        getBasePage().implicitWait(300);
        getBasePage().clickOnCart();
        getBasePage().waitForPageLoadComplete(60);
        getCart().clickOnRemoveItemButton();
        getBasePage().waitForPageLoadComplete(60);
        getHomePage().clickOnWomanCatalogButton();
        getBasePage().implicitWait(60);
        Assert.assertEquals(getHomePage().emptyCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_REMOVING_ITEM);

    }

}
