package pagefactory.tests;

import org.testng.annotations.Test;

public class SignInTests extends BaseTest {

    private String EMAIL = "inna@gmail.com";
    private String PASSWORD = "12345";

    @Test(priority = 1)
    public void checkSignInWithEmptyEmailAndPassword() {
        getBasePage().signIn();
        getBasePage().waitForPageLoadComplete(300);
        getLoginPage().clickOnSignInButton();
        getBasePage().waitForPageLoadComplete(300);
        getLoginPage().getSearchResultMessageEmptyFields();
    }

    @Test(priority = 2)
    public void checkSignInWithIncorrectEmailAndPassword() {
        getBasePage().signIn();
        getBasePage().waitForPageLoadComplete(180);
        getLoginPage().inputWrongDataToSignInFields(EMAIL, PASSWORD);
        getLoginPage().clickOnSignInButton();
        getBasePage().waitForPageLoadComplete(180);
    }
}
