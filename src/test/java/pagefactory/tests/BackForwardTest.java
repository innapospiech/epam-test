package pagefactory.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BackForwardTest extends BaseTest {

    private static final String ghdStoreTitle = "GHDs| Ceramic Hair Straighteners, Mini Stylers & Paddle Hairbrushes | ASOS";

    @Test
    public void checkBackForwardOnPage() {
        getHomePage().clickOnWomanCatalogButton();
        getGhdStylingPage().clickBackButton(120);
        getHomePage().clickOnGhdStylingButton();
        getGhdStylingPage().clickBackButton(60);
        getHomePage().clickForwardButton(60);
        Assert.assertEquals(ghdStoreTitle, getGhdStylingPage().getPageTitle());
    }

}
