package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {

    private String NOTHING_MATCHES_MESSAGE = "NOTHING MATCHES YOUR SEARCH";

    @FindBy(xpath = "//span[@class='_16nzq18']")
    private List<WebElement> searchResultsProductsListText;

    @FindBy(xpath = "//section[contains(@class,'grid-backgroundWrapper__row')]//h2[@class='grid-text__title ']")
    private WebElement nothingMatchesMessage;


    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public int getSearchResultCount() {
        return getSearchResultsList().size();
    }

    public List<WebElement> getSearchResultsList() {
        return searchResultsProductsListText;
    }

    public boolean getSearchResultMessageNothingMatches() {
        return (nothingMatchesMessage.getText().contentEquals(NOTHING_MATCHES_MESSAGE));
    }
}
