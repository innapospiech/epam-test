package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GhdHotBrushPage extends BasePage {

    @FindBy(xpath = "//a[@class='add-button']")
    private WebElement addToCartButton;

    public GhdHotBrushPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }
}
