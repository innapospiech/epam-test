package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    private String EMPTY_EMAIL_MESSAGE = "Oops! You need to type your email here";
    private String EMPTY_PASSWORD_MESSAGE = "Hey, we need a password here";
    private String WRONG_DATA_MESSAGE = "Looks like either your email address or password were incorrect. Wanna try again?";

    @FindBy(xpath = "//input[@id='signin']")
    private WebElement getSignInButton;

    @FindBy(xpath = "//span[@id='EmailAddress-error']")
    private WebElement emptyEmailMessage;

    @FindBy(xpath = "//span[@id='Password-error']")
    private WebElement emptyPasswordMessage;

    @FindBy(xpath = "//input[@id='EmailAddress']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='Password']")
    private WebElement passwordField;

    @FindBy(xpath = "//li[@id='loginErrorMessage']")
    private WebElement wrongDataMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public boolean getSearchResultMessageEmptyFields() {
        return emptyEmailMessage.getText() == EMPTY_EMAIL_MESSAGE && emptyPasswordMessage.getText() == EMPTY_PASSWORD_MESSAGE;
    }

    public void clickOnSignInButton() {
        getSignInButton.click();
    }

    public void inputWrongDataToSignInFields(final String email, final String password) {
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
    }

    public boolean getSignInMessageWrongData() {
        return wrongDataMessage.getText() == WRONG_DATA_MESSAGE;
    }

}
