package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GhdStylingPage extends BasePage {

    @FindBy(xpath = "//img[@alt='ghd rise professional hot brush UK plug']")
    private WebElement ghdHotBrushButton;

    public GhdStylingPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnGhdHotBrushButton() {
        ghdHotBrushButton.click();
    }

}
