package pagefactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Cart extends BasePage {

    @FindBy(xpath = "//button[@aria-label='Delete this item']")
    private WebElement removeItemButton;

    public Cart(WebDriver driver) {
        super(driver);
    }

    public void clickOnRemoveItemButton() {
        removeItemButton.click();
    }
}
