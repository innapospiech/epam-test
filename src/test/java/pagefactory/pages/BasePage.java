package pagefactory.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BasePage {

    WebDriver driver;

    @FindBy(xpath = "//a[@data-testid='bagIcon']")
    private WebElement cartButton;

    @FindBy(xpath = "//button[@data-testid='accountIcon']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@data-testid='signin-link']")
    private WebElement signInLink;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void implicitWait(long timeToWait) {
        driver.manage().timeouts().implicitlyWait(timeToWait, TimeUnit.SECONDS);
    }

    public void waitForPageLoadComplete(long timeToWait) {
        new WebDriverWait(driver, timeToWait).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public void clickOnCart() {
        cartButton.click();
    }

    public void clickBackButton(long timeToWait) {
        driver.navigate().back();
        waitForPageLoadComplete(timeToWait);
    }

    public void clickForwardButton(long timeToWait) {
        driver.navigate().forward();
        waitForPageLoadComplete(timeToWait);
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void signIn() {
        signInButton.click();
        waitForPageLoadComplete(60);
        signInLink.click();
    }

}
