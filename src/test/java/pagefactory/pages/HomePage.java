package pagefactory.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@id='chrome-search']")
    private WebElement searchInput;

    @FindBy(xpath = "//a[@data-testid='women-floor']")
    private WebElement productWomanCatalogButton;

    @FindBy(xpath = "//a[@data-analytics-id='com-gblghdstyling-mulink']")
    private WebElement ghdStylingButton;

    @FindBy(xpath = "//span[@class='_1z5n7CN']")
    private WebElement amountOfProductsInCart;

    @FindBy(xpath = "//span[@class='AckDUvD -rhP1cz gBrrjX4 _2nHArcS']")
    private WebElement emptyCart;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickOnWomanCatalogButton() {
        productWomanCatalogButton.click();
    }

    public void clickOnGhdStylingButton() {
        ghdStylingButton.click();
    }

    public String getTextOfAmountProductsInCart() {
        return amountOfProductsInCart.getText();
    }

    public String emptyCart() {
        return emptyCart.getText();
    }


}
